import sys
import math
import numpy as np
import pandas as pd
from collections import Counter
from sklearn import tree
from pprint import pprint

def getMode(data):
    return data.loc[:,'classifier'].mode()[0]

def getAttribute(features, data):
    infGain = [getInfo(data, feature) for feature in features]
    bestAttrIndex = np.argmax(infGain)
    bestAttr = features[bestAttrIndex] # get the one with the highest Information Gain
    return bestAttr

def getEntropy(x):
    return sum([-p * math.log(p, 2) for p in x]) # use the entropy formula

def calcEntropy(column): # calculate entropy for specified column
    count = Counter(attr for attr in column)
    num_instances = len(column) * 1.0
    probs = [attr / num_instances for attr in count.values()]
    return getEntropy(probs)

def getInfo(data, column):
    df_split = data.groupby(column)
    
    length = len(data.index) * 1.0
    df_agg_ent = df_split.agg({'classifier' : [calcEntropy, lambda x: len(x)/length] })['classifier']
    df_agg_ent.columns = ['Entropy', 'PropObservations']
    
    newEntropy = sum(df_agg_ent['Entropy'] * df_agg_ent['PropObservations'])
    oldEntropy = calcEntropy(data['classifier'])
    return oldEntropy - newEntropy

def classify(tree, datapoint, classifiers):
    root = list(tree.keys())[0]
    if tree[root][datapoint.iloc[0][root]] in classifiers:
        classification = tree[root][datapoint.iloc[0][root]]
        return classification
    else:
        return classify(tree[root][datapoint.iloc[0][root]], datapoint, classifiers)

def accuracy(prediction, acutal):
    count = 0
    for i in range(len(prediction)):
        if prediction[i] == acutal[i]:
            count += 1
    return count/len(prediction) * 100

def ID3(data, features, defaultVal): # build the tree
    if len(features) == 0 or data.empty:
        return defaultVal # return default value if empty
    elif len(data['classifier'].unique()) == 1:
        return data.iloc[0]['classifier'] # if there's only one classifier
    else:
        bestFeature = getAttribute(features, data)
        tree = {bestFeature:{}}
        bestAttributes = data[bestFeature].unique()
        features.remove(bestFeature) # remove best feature from main feature list
        for attr in bestAttributes:
            newData = data.loc[data[bestFeature] == attr]
            subtree = ID3(newData, features, getMode(data)) # recursively create subnodes with remaining attributes
            tree[bestFeature][attr] = subtree
        return tree

def main():
    data = pd.read_csv(sys.argv[1])
    
    #test_vals = list(alg2['classifier'])
    classifiers = data['classifier'].unique()
    #alg2 = alg2.drop(['classifier'], axis=1)
    features = list(data.columns)
    features.remove('classifier')
    default_value = getMode(data)
    
    tree = ID3(data, features, default_value)
    pprint(tree)
    
    prediction = []
    #for index, row in alg2.iterrows():
    #    prediction.append(classify(tree, alg2.iloc[[index]], classifiers))

    #print("Accuracy Level:", accuracy(prediction, test_vals), "%")

if __name__ == "__main__":
    main()
