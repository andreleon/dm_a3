library("ggplot2")
setwd("C:/Users/bowga/Documents/GitHub/master/data")
diabetes <- read.csv("Pima Indians Diabetes.csv")
diabetes.f <- diabetes
diabetes.stand <- scale(diabetes[-1])
k.means.fit <- kmeans(diabetes.stand, 4)
attributes(k.means.fit)

library(cluster)
clusplot(diabetes.stand, k.means.fit$cluster, main='2D representation of the Cluster solution',
         color=TRUE, shade=TRUE,
         labels=2, lines=0)